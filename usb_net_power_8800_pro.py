#!/usr/bin/env python

#    Copyright (C) 2017 Keith Simmons (ksimmons@gmail.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


# usb_net_power_8800_pro.py - Control USB Net Power 8800 Pro devices using 
# serial
#
# https://bitbucket.org/oneironaut/usbnetpower8800pro/
# 
# EXAMPLES:
# turn on outlet: 
#	./usb_net_power_8800_pro.py -s on
# turn off outlet specifying serial parameters: 
#	./usb_net_power_8800_pro.py -p /dev/ttyACM0 -b 9600 -s off
#
# NOTES:
# Simply sending '@' turns outlet one on and '!' turns it off but this behavior 
# is undocumented.  This script uses documented long command syntax 
#
# This software is designed to work with multi-outlet devices but only tested 
# with a version that has one physical outlet

import sys, argparse, serial, serial.tools.list_ports
from time import sleep

parser = argparse.ArgumentParser(description="Control USB Net Power 8800 Pro devices using serial", epilog="example usage to turn on outlet: '%(prog)s -s on'")
parser.add_argument("-p", "--port", default="/dev/ttyACM0", help="desired serial port (default is /dev/ttyACM0)")
parser.add_argument("-b", "--baud", type=int, default=19200, help="desired baud rate for serial port (default is 19200)")
parser.add_argument("-o", "--outlet", type=int, default=1, help="outlet channel to set or query (default is 1)")
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument("--list", "-l", action="store_true", help="list available serial ports (may be incomplete)")
group.add_argument("-q", "--query", action="store_true",  help="query the state of the relay")
group.add_argument("-s", "--set", choices=["on", "off"], help="set the state of the relay to on or off")
args = parser.parse_args()

# List serial ports and exit
if args.list:
	print "Listing serial ports (this list may be incomplete):"
	ports = list(serial.tools.list_ports.comports())
	for p in ports:
	    print p
	sys.exit(0)	

# Open serial; timeout specifies maximum time for readline(s)() calls
ser = serial.Serial(args.port, args.baud, timeout=0.5)

# Bitmask lets us check a specific outlet channel
# To get outlet bitmask, do 2**(outlet-1)
outlet_bitmask = 2**(args.outlet-1)

if args.query:
	# Ask Usb Net Power 8800 Pro what the state is 
	ser.write('readio\r\n')
	rawIoState = int (ser.readlines()[1].split(":")[1])

	# Find IO state of one port based on value
	maskedIoState = rawIoState & outlet_bitmask # think of bitmask like this: 0b0001
	if maskedIoState == 0:
		print "outlet p%d is off" % args.outlet
	else:
		print "outlet p%d is on" % args.outlet
elif args.set:
	power_state = 1 if args.set == "on" else 0
	print "outlet p%d set to %s" % (args.outlet, args.set)
	set_msg = "p%d=%d\r\n" % (args.outlet, power_state)
	ser.write(set_msg)

ser.close()

