# README #

The USB Net Power 8800 Pro is a usb controlled switching power outlet.  This script allows you to control USB Net Power 8800 Pro devices using serial emulation from the command line.  Other software designed for the non-Pro version did not seem to work with my device; this is designed to use a serial interface to set and query the power state of the outlet.

USB Net Power 8800 Pro is reported by lsusb in linux as: "ID 04d8:000a Microchip Technology, Inc. CDC RS-232 Emulation Demo".  This is a different ID from the non-pro versions and does not seem to be compatible with the same usb control transfer messages used in other programs.
  
Serial emulation used in this script *should* work for all devices (pro or non-pro) that support serial port emulation.

Manual for USB Net Power 8800 Pro:  
http://www.aviosys.com/downloads/manuals/power/USB%20Net%20Power%208800%20Pro%20Manual_EN.pdf

```text
usage: usb_net_power_8800_pro.py [-h] [-p PORT] [-b BAUD] [-o OUTLET]
                                 (--list | -q | -s {on,off})

Control USB Net Power 8800 Pro devices using serial

optional arguments:
  -h, --help            show this help message and exit
  -p PORT, --port PORT  desired serial port (default is /dev/ttyACM0)
  -b BAUD, --baud BAUD  desired baud rate for serial port (default is 19200)
  -o OUTLET, --outlet OUTLET
                        outlet channel to set or query (default is 1)
  --list, -l            list available serial ports (may be incomplete)
  -q, --query           query the state of the relay
  -s {on,off}, --set {on,off}
                        set the state of the relay to on or off

example usage to turn on outlet: 'usb_net_power_8800_pro.py -s on'

```



### How do I get set up? ###

This script requires that python is installed on the controlling computer.

### Limitations ###

Serial emulation mode does not appear to support temperature detection, current meter, or current protection (limit) that is advertised on the packaging.

### See Also ###
For non-pro USB Net Power 8800 devices, this project uses usb control transfer to skip serial emulation:  
https://github.com/pmarks-net/usbnetpower8800